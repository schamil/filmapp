package com.shamildev.retro.data.net.error;

import java.io.IOException;

/**
 * Created by R_KAY on 11/21/2017.
 */

public class EmptyDatasetException extends IOException {

    private final String message;

    public EmptyDatasetException(){
        this.message = "No Data";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
