package com.shamildev.retro.data.net;

/**
 * Created by Shamil Lazar on 15.12.2017.
 */

public class UrlManager {


    public static final String BASE_URL_MDB = "https://api.themoviedb.org/";
    public static final String PUBLIC_KEY_MDB = "96306bc3cc12ed9ef756ba9a85628586";

    public static final String REPOS = "/users/{user}/repos";
    public static final String API_HOST = "https://api.github.com";

}
