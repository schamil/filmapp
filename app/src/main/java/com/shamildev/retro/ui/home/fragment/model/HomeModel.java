package com.shamildev.retro.ui.home.fragment.model;

import com.shamildev.retro.ui.common.model.BaseModel;



/**
 * Created by Shamil Lazar.
 */


public abstract class HomeModel extends BaseModel<HomeModelImpl> {




    public abstract void initData();


}